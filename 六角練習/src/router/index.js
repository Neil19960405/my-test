import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '@/views/pages/login.vue'
import Dashboard from '@/views/dashBoard.vue'
import Products from '@/views/pages/Product.vue'
import CustomerOrder from '@/components/CustomerOrder.vue'
import cupon from '@/components/cupon.vue'
import CustomerCheck from '@/components/CustomerCheck.vue'
import OrderList from '@/components/OrderList.vue'
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta:{requiresAuth:true},
  },
  {
    path: '*',
    redirect:'login'
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta:{requiresAuth:true},
    children:[
      {
        path:'products',
        name:'Products',
        component:Products,
        meta:{requiresAuth:true}
      }
    ]
  },
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
    meta:{requiresAuth:true},
    children:[
      {
        path:'CustomerOrder',
        name:'CustomerOrder',
        component:CustomerOrder,
        meta:{requiresAuth:true}
      },
      {
        path:'cupon',
        name:'cupon',
        component:cupon,
        meta:{requiresAuth:true}
      },
      {
        path:'CustomerOrder/:orderId',
        name:'CustomerCheck',
        component:CustomerCheck,
        meta:{requiresAuth:true}
      },
      {
        path:'OrderList',
        name:'OrderList',
        component:OrderList,
        meta:{requiresAuth:true}
      }

    ]
  },

]

const router = new VueRouter({
  routes
})

export default router
