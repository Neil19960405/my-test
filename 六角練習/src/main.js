import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'bootstrap';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Loading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'
import dollar from '@/components/dollar.js'
import '@/bus'
import * as VeeValidate from 'vee-validate'
import zhTWValidate from 'vee-validate/dist/locale/zh_TW'
Vue.config.productionTip = false
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.component('Loading',Loading)
axios.defaults.withCredentials=true;
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueAxios , axios);
Vue.filter('dollar',dollar);

//設定vee-validate
Vue.use(VeeValidate);
VeeValidate.Validator.localize('zh_TW',zhTWValidate)
// Add a rule.


// Register it globally



//設定完成
new Vue({
 
  router,
  store,
  render: h => h(App)
}).$mount('#app')

router.beforeEach((to, from, next) => {
  if(to.meta.requiresAuth){
    const api ="https://vue-course-api.hexschool.io/api/user/check";
    axios.post(api).then( (response)=>{
      console.log(response.data)
      
      
      if(response.data.success){
        next();
      }else{
        next({
          path:'/login'
        })
      }

    })
  }
  
  else{next();}

   
})